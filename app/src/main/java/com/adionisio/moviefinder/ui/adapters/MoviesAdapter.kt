package com.adionisio.moviefinder.ui.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.adionisio.moviefinder.databinding.MovieItemViewHolderBinding
import com.adionisio.moviefinder.ui.extensions.fadeIn
import com.adionisio.moviefinder.ui.extensions.loadMovieFromUrl
import com.adionisio.moviefinder.ui.models.MovieUI

class MoviesAdapter(private val listener: (MovieUI) -> Unit) : RecyclerView.Adapter<MoviesAdapter.MoviesViewHolder>() {

    private val differCallback = object : DiffUtil.ItemCallback<MovieUI>() {
        override fun areItemsTheSame(oldItem: MovieUI, newItem: MovieUI): Boolean {
            return oldItem.title  == newItem.title
        }

        override fun areContentsTheSame(oldItem: MovieUI, newItem: MovieUI): Boolean {
            return oldItem == newItem
        }

    }

    val differ = AsyncListDiffer(this, differCallback)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MoviesViewHolder {
        val binding = MovieItemViewHolderBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return MoviesViewHolder(binding, listener)
    }

    override fun getItemCount(): Int {
        return differ.currentList.size
    }

    override fun onBindViewHolder(holder: MoviesViewHolder, position: Int) {
        holder.bind(differ.currentList[position])
    }

    class MoviesViewHolder(private val binding: MovieItemViewHolderBinding, private val listener: (MovieUI) -> Unit) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(movie: MovieUI) {
            binding.apply {
                imagePosterMovie.loadMovieFromUrl(movie.posterPath)
                imagePosterMovie.setOnClickListener { listener(movie) }
            }
        }
    }

}