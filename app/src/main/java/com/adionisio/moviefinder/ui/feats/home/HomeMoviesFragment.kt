package com.adionisio.moviefinder.ui.feats.home

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager.HORIZONTAL
import com.adionisio.moviefinder.MovieFinderApplication
import com.adionisio.moviefinder.R
import com.adionisio.moviefinder.databinding.FragmentHomeMoviesBinding
import com.adionisio.moviefinder.ui.MoviesActivity
import com.adionisio.moviefinder.ui.adapters.MoviePaginationListener
import com.adionisio.moviefinder.ui.adapters.MoviesAdapter
import com.adionisio.moviefinder.ui.adapters.MoviesRatedAverageAdapter
import com.adionisio.moviefinder.ui.extensions.loadMovieFromUrl
import com.adionisio.moviefinder.ui.models.MovieUI
import javax.inject.Inject


class HomeMoviesFragment : Fragment() {

    @Inject
    lateinit var viewModelHome: HomeMoviesViewModel
    private lateinit var binding: FragmentHomeMoviesBinding
    private lateinit var moviesPopularAdapter: MoviesAdapter
    private lateinit var moviesPopularPaginationListener: MoviePaginationListener
    private lateinit var moviesMostRatedAdapter: MoviesRatedAverageAdapter
    private lateinit var moviesMostRatedPaginationListener: MoviePaginationListener
    private lateinit var moviesUpcomingAdapter: MoviesAdapter
    private lateinit var moviesUpcomingPaginationListener: MoviePaginationListener
    private lateinit var popularLayoutManager: LinearLayoutManager
    private lateinit var mostRatedLayoutManager: LinearLayoutManager
    private lateinit var upcomingLayoutManager: LinearLayoutManager

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentHomeMoviesBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupRecyclerView()
        setupObservers()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        (requireActivity().application as MovieFinderApplication).appComponent.inject(this)
    }

    override fun onResume() {
        super.onResume()
        val activity = requireActivity() as? MoviesActivity
        activity?.toolbarHandler?.showToolbar()
    }

    override fun onPause() {
        super.onPause()
        viewModelHome.apply {
            stateListPopular = popularLayoutManager.onSaveInstanceState()
            stateListMostRated = mostRatedLayoutManager.onSaveInstanceState()
            stateListUpcoming = upcomingLayoutManager.onSaveInstanceState()
        }
    }

    private fun setupObservers() {
        viewModelHome.state.observe(viewLifecycleOwner, Observer { renderState(it) })
    }

    private fun renderState(state: HomeMoviesViewModel.HomeState) {
        with(state) {
            binding.apply {
                loadingMostRatedBar.visibility = if (loadingPopular) View.VISIBLE else View.GONE
                loadingLastestBar.visibility = if (loadingMostRated) View.VISIBLE else View.GONE
                loadingUpcomingBar.visibility = if (loadingUpcoming) View.VISIBLE else View.GONE
                loadingRandomPopularImageBar.visibility = if (loadingRandomPopularMovie) View.VISIBLE else View.GONE
                moviesPopularPaginationListener.isLoading = loadingPopular
                moviesMostRatedPaginationListener.isLoading = loadingMostRated
                moviesUpcomingPaginationListener.isLoading = loadingUpcoming
                state.randomPopularMovie?.let { showMostRandomPopularMovie(it) }
                showPopularList(state.popularMoviesList)
                showMostRatedList(state.moviesMostRatedList)
                showUpcomingList(state.moviesUpcomingList)
                if (errorMoviesNotFound) {
                    handleErrorMoviesNotFound()
                }
                if (errorNetworkUnavailable) {
                    handleErrorMoviesNetworkUnavailable()
                }
            }
        }
    }

    private fun showMostRandomPopularMovie(randomPopularMovie: MovieUI) {
        binding.randomPopularImageView.apply {
            loadMovieFromUrl(randomPopularMovie.posterPath)
            setOnClickListener { goToDetail(randomPopularMovie) }
        }
    }

    private fun showMostRatedList(moviesMostRatedList: List<MovieUI>) {
        moviesMostRatedAdapter.differ.submitList(moviesMostRatedList)
        binding.mostRatedListLabel.visibility = View.VISIBLE
        binding.mostRatedRecyclerView.visibility = View.VISIBLE
    }

    private fun showUpcomingList(moviesUpcomingList: List<MovieUI>) {
        moviesUpcomingAdapter.differ.submitList(moviesUpcomingList)
        binding.upcomingRecyclerView.visibility = View.VISIBLE
        binding.upcomingMoviesListLabel.visibility = View.VISIBLE
    }

    private fun showPopularList(moviesList: List<MovieUI>) {
        moviesPopularAdapter.differ.submitList(moviesList)
        binding.popularMoviesListLabel.visibility = View.VISIBLE
        binding.popularRecyclerView.visibility = View.VISIBLE
    }

    private fun handleErrorMoviesNetworkUnavailable() {
        binding.popularErrorInfo.text = getString(R.string.network_unavailable)
        showError()
    }

    private fun handleErrorMoviesNotFound() {
        binding.popularErrorInfo.apply {
            text = getString(R.string.movies_not_found)
            setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0)
            showError()
        }
    }

    private fun showError() {
        binding.apply {
            lastMoviesContent.visibility = View.GONE
            upcomingMoviesContent.visibility = View.GONE
            popularRatedContent.visibility = View.GONE
            randomPopularImageView.visibility = View.GONE
            popularErrorInfo.visibility = View.VISIBLE
        }
    }

    private fun setupRecyclerView() {
        setupPopularRecyclerView()
        setupUpcomingRecyclerView()
        setupMostRatedRecyclerView()
    }

    private fun setupPopularRecyclerView() {
        moviesPopularAdapter = MoviesAdapter { goToDetail(it) }
        binding.popularRecyclerView.apply {
            adapter = moviesPopularAdapter
            popularLayoutManager = LinearLayoutManager(activity, HORIZONTAL, false)
            layoutManager = popularLayoutManager
            moviesPopularPaginationListener = MoviePaginationListener(
                layoutManager as LinearLayoutManager,
                isLastPage = false,
                isLoading = false,
                callback = ::onLoadMorePopularMovies
            )
            addOnScrollListener(moviesPopularPaginationListener)
            viewModelHome.stateListPopular?.let { popularLayoutManager.onRestoreInstanceState(it) }
        }
    }

    private fun setupMostRatedRecyclerView() {
        moviesMostRatedAdapter = MoviesRatedAverageAdapter { goToDetail(it) }
        binding.mostRatedRecyclerView.apply {
            adapter = moviesMostRatedAdapter
            mostRatedLayoutManager = LinearLayoutManager(activity, HORIZONTAL, false)
            layoutManager = mostRatedLayoutManager
            moviesMostRatedPaginationListener = MoviePaginationListener(
                layoutManager as LinearLayoutManager,
                isLastPage = false,
                isLoading = false,
                callback = ::onLoadMostRatedMovies
            )
            addOnScrollListener(moviesMostRatedPaginationListener)
            viewModelHome.stateListMostRated?.let { mostRatedLayoutManager.onRestoreInstanceState(it) }
        }
    }

    private fun setupUpcomingRecyclerView() {
        moviesUpcomingAdapter = MoviesAdapter { goToDetail(it) }
        binding.upcomingRecyclerView.apply {
            adapter = moviesUpcomingAdapter
            upcomingLayoutManager = LinearLayoutManager(activity, HORIZONTAL, false)
            layoutManager = upcomingLayoutManager
            moviesUpcomingPaginationListener = MoviePaginationListener(
                layoutManager as LinearLayoutManager,
                isLastPage = false,
                isLoading = false,
                callback = ::onLoadUpcomingMovies
            )
            addOnScrollListener(moviesUpcomingPaginationListener)
            viewModelHome.stateListUpcoming?.let { upcomingLayoutManager.onRestoreInstanceState(it) }
        }
    }

    private fun onLoadMorePopularMovies() {
        viewModelHome.getMorePopularMovies()
    }

    private fun onLoadMostRatedMovies() {
        viewModelHome.getMostRatedMovies()
    }

    private fun onLoadUpcomingMovies() {
        viewModelHome.getUpcomingMovies()
    }

    private fun goToDetail(movie: MovieUI) {
        val action =
            HomeMoviesFragmentDirections.actionHomeMoviesFragmentToMovieDetailFragment(
                movie
            )
        findNavController().navigate(action)
    }

}