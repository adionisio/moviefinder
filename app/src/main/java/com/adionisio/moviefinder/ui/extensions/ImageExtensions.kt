package com.adionisio.moviefinder.ui.extensions

import android.widget.ImageView
import com.adionisio.moviefinder.config.ConfigConstants.BASE_IMAGE_URL
import com.bumptech.glide.Glide
import com.bumptech.glide.load.MultiTransformation
import jp.wasabeef.glide.transformations.BlurTransformation
import jp.wasabeef.glide.transformations.CropSquareTransformation
import jp.wasabeef.glide.transformations.gpu.VignetteFilterTransformation

fun ImageView.loadMovieFromUrl(url: String) =
    Glide.with(context).load(BASE_IMAGE_URL + url).centerCrop().into(this)
fun ImageView.loadBlurMovieFromUrl(url: String) {
    this.alpha = 0f
    Glide.with(context)
        .load(BASE_IMAGE_URL + url)
        .centerCrop()
        .transform(
            MultiTransformation(
                BlurTransformation(25, 2),
                VignetteFilterTransformation()
            )
        )
        .into(this)
    this.animate().alpha(1f).withEndAction { }.setDuration(2000).start()
}
fun ImageView.loadMovieCircleFromUrl(url: String) =
    Glide.with(context).load(BASE_IMAGE_URL + url).centerCrop().circleCrop().into(this)
fun ImageView.fadeIn(atEnd: () -> Unit) =
    this.animate().alpha(1f).withEndAction { atEnd() }.setDuration(1000).start()