package com.adionisio.moviefinder.ui.feats.details

import androidx.lifecycle.viewModelScope
import com.adionisio.moviefinder.Error
import com.adionisio.moviefinder.MovieRepository
import com.adionisio.moviefinder.ui.bases.BaseState
import com.adionisio.moviefinder.ui.bases.BaseViewModel
import com.adionisio.moviefinder.ui.models.MovieDetailsUI
import com.adionisio.moviefinder.ui.models.MovieUI
import com.adionisio.moviefinder.ui.models.mapper.MapperUI.moviesUImapperToMoviesDomain
import com.adionisio.moviefinder.ui.models.movieDetailsUImapperFromMovieDetailDomain
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

class MovieDetailViewModel @Inject constructor(private val repository: MovieRepository) :
    BaseViewModel<MovieDetailViewModel.MovieDetailState>() {

    data class MovieDetailState(
        val loading: Boolean = false,
        val movieDetails: MovieDetailsUI? = null,
        val error: Boolean = false
    ) : BaseState

    init {
        _state.value = MovieDetailState()
    }

    fun getInitialData(movieId: Int) {
        _state.value = _state.value?.copy(loading = true)
        viewModelScope.launch {
            val movieDetailsReturn = repository.getMovieDetails(movieId)
            movieDetailsReturn.fold(::handleFailure) { movieDetails ->
                _state.value = _state.value?.copy(
                    loading = false,
                    movieDetails = movieDetailsUImapperFromMovieDetailDomain(movieDetails)
                )
            }
        }
    }

    fun saveMovie(movieUI: MovieUI) {
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                repository.saveMovie(moviesUImapperToMoviesDomain(movieUI))
            }
        }
    }

    private fun handleFailure(error: Error) {
        _state.value = _state.value?.copy(loading = false, error = true)
    }
}