package com.adionisio.moviefinder.ui.models

import com.adionisio.moviefinder.domain.MovieDetails

data class MovieDetailsUI(
    val genres: List<String>,
    val homepage: String,
    val id: Int,
    val imdbId: String,
    val originalLanguage: String,
    val originalTitle: String,
    val runtime: Int,
    val spokenLanguages: List<String>,
    val tagline: String,
    val video: String?
)

fun movieDetailsUImapperFromMovieDetailDomain(movieDetail: MovieDetails): MovieDetailsUI {
    return MovieDetailsUI(
        movieDetail.genres,
        movieDetail.homepage,
        movieDetail.id,
        movieDetail.imdbId,
        movieDetail.originalLanguage,
        movieDetail.originalTitle,
        movieDetail.runtime,
        movieDetail.spokenLanguages,
        movieDetail.tagline,
        movieDetail.video
    )

}