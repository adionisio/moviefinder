package com.adionisio.moviefinder.ui.feats.saved

import android.content.Context
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.adionisio.moviefinder.MovieFinderApplication
import com.adionisio.moviefinder.R
import com.adionisio.moviefinder.databinding.FragmentSavedMoviesBinding
import com.adionisio.moviefinder.ui.MoviesActivity
import com.adionisio.moviefinder.ui.adapters.SavedMoviesAdapter
import com.adionisio.moviefinder.ui.feats.home.HomeMoviesFragmentDirections
import com.adionisio.moviefinder.ui.models.MovieUI
import javax.inject.Inject

class SavedMoviesFragment : Fragment() {

    @Inject
    lateinit var viewModel: SavedMoviesViewModel
    private lateinit var binding: FragmentSavedMoviesBinding
    private lateinit var savedMoviesAdapter: SavedMoviesAdapter
    private lateinit var savedMoviesLayoutManager: LinearLayoutManager


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentSavedMoviesBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupRecyclerView()
        setupObservers()
    }

    private fun setupRecyclerView() {
        savedMoviesAdapter = SavedMoviesAdapter { goToDetail(it) }
        binding.savedMoviesRecycler.apply {
            adapter = savedMoviesAdapter
            savedMoviesLayoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
            layoutManager = savedMoviesLayoutManager
        }
        viewModel.stateListSaved?.let { savedMoviesLayoutManager.onRestoreInstanceState(it) }
    }

    override fun onResume() {
        super.onResume()
        val activity = requireActivity() as? MoviesActivity
        activity?.toolbarHandler?.showToolbar()
    }

    override fun onPause() {
        super.onPause()
        viewModel.stateListSaved = savedMoviesLayoutManager.onSaveInstanceState()
    }

    private fun setupObservers() {
        viewModel.state.observe(viewLifecycleOwner, Observer { renderState(it) } )
    }

    private fun renderState(state: SavedMoviesViewModel.SavedMoviesState) {
        with(state){
            binding.apply {
                loadingSavedMovies.visibility = if (loading) View.VISIBLE else View.GONE
                if (error){ errorSavedMoviesTextView.text = resources.getString(R.string.saved_movies_error) }
                if (notFoundError){ errorSavedMoviesTextView.text = resources.getString(R.string.saved_movies_not_found) }
                errorSavedMoviesTextView.visibility = if (error) View.VISIBLE else View.GONE
                errorSavedMoviesTextView.visibility = if (notFoundError) View.VISIBLE else View.GONE
                if (savedMoviesList.isNotEmpty()){
                    savedMoviesAdapter.differ.submitList(savedMoviesList)
                    savedMoviesList.map { Log.v("SAVEDMOVIES", it.title) }
                    savedMoviesRecycler.visibility = View.VISIBLE
                }
            }
        }
    }

    private fun goToDetail(movie: MovieUI) {
        val action =
            SavedMoviesFragmentDirections.actionSavedMoviesFragmentToMovieDetailFragment(
                movie
            )
        findNavController().navigate(action)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        (requireActivity().application as MovieFinderApplication).appComponent.inject(this)
    }

}