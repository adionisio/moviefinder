package com.adionisio.moviefinder.ui.models

import com.adionisio.moviefinder.domain.Movie
import java.io.Serializable
import java.util.*

data class MovieUI(
    val id: Int,
    val title: String,
    val overview: String,
    val posterPath: String,
    val voteAverage: Double,
    val releaseDate: Date,
    val voteCount: Int,
    val adult: Boolean

) : Serializable