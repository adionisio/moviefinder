package com.adionisio.moviefinder.ui.feats.details

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.navArgs
import androidx.navigation.ui.NavigationUI
import com.adionisio.moviefinder.MovieFinderApplication
import com.adionisio.moviefinder.R
import com.adionisio.moviefinder.databinding.FragmentMovieDetailBinding
import com.adionisio.moviefinder.ui.MoviesActivity
import com.adionisio.moviefinder.ui.extensions.loadBlurMovieFromUrl
import com.adionisio.moviefinder.ui.extensions.loadMovieFromUrl
import com.adionisio.moviefinder.ui.models.MovieDetailsUI
import javax.inject.Inject


class MovieDetailFragment : Fragment() {

    @Inject
    lateinit var viewModel: MovieDetailViewModel
    private val args: MovieDetailFragmentArgs by navArgs()
    private lateinit var binding: FragmentMovieDetailBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentMovieDetailBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val navHostFragment = NavHostFragment.findNavController(this);
        NavigationUI.setupWithNavController(binding.toolbarDetails, navHostFragment)
        binding.toolbarDetails.title = ""
        setupObservers()
        viewModel.getInitialData(args.movie.id)
    }

    override fun onResume() {
        super.onResume()
        val activity = requireActivity() as? MoviesActivity
        activity?.toolbarHandler?.hideToolbar()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        val activity = requireActivity() as? MoviesActivity
        activity?.toolbarHandler?.showToolbarLogo()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        (requireActivity().application as MovieFinderApplication).appComponent.inject(this)
    }

    private fun saveMovie() {
        val savedText = resources.getString(R.string.movie_saved, args.movie.title)
        Toast.makeText(context, savedText, Toast.LENGTH_SHORT).show()
        viewModel.saveMovie(args.movie)
    }

    private fun setupObservers() {
        viewModel.state.observe(viewLifecycleOwner, Observer { renderState(it) })
    }

    private fun renderState(state: MovieDetailViewModel.MovieDetailState) {
        with(state) {
            binding.loadingBar.visibility = if (state.loading) View.VISIBLE else View.GONE
            state.movieDetails?.let { showMoviesDetails(it) }
            if (state.error) { handleError() }
        }
    }

    private fun showMoviesDetails(movieDetails: MovieDetailsUI) {
        Log.v("MovieDetailResult", movieDetails.toString())
        binding.apply {
            genresTextView.text = movieDetails.genres.joinToString(", ")
            imdbImageButton.setOnClickListener { navigateToWeb(movieDetails.imdbId) }
            webTextView.setOnClickListener { navigateToWeb(movieDetails.homepage) }
            movieDetails.video?.let {
                val webIntent = Intent(Intent.ACTION_VIEW, Uri.parse(it))
                youtubeButton.setOnClickListener { activity?.startActivity(webIntent) }
                youtubeButton.visibility = View.VISIBLE
            }
            genresTextView.visibility = View.VISIBLE
            imdbImageButton.visibility = View.VISIBLE
            webTextView.visibility = View.VISIBLE
        }
        showMovieArgsData()

    }

    @SuppressLint("RestrictedApi")
    private fun showMovieArgsData() {
        binding.apply {
            movieDetailImage.loadMovieFromUrl(args.movie.posterPath)
            movieDetailImageBg.loadBlurMovieFromUrl(args.movie.posterPath)
            overviewTextView.text = args.movie.overview
            overviewTextView.visibility = View.VISIBLE
            voteAverageTextView.text = args.movie.voteAverage.toString()
            voteAverageTextView.visibility = View.VISIBLE
            votesCountsTextView.text =
                resources.getString(R.string.vote_counts, args.movie.voteCount.toString())
            adultIndicator.visibility = if (args.movie.adult) View.VISIBLE else View.GONE
            saveMovieActionButton.setOnClickListener { saveMovie() }
            saveMovieActionButton.visibility = View.VISIBLE
        }
    }

    private fun navigateToWeb(url: String) {
        val intent = Intent(Intent.ACTION_VIEW)
        intent.data = Uri.parse(url)
        startActivity(intent)
    }

    private fun handleError() {
        binding.apply {
            genresTextView.visibility = View.INVISIBLE
            imdbImageButton.visibility = View.GONE
            webTextView.visibility = View.GONE
        }
        showMovieArgsData()
    }
}