package com.adionisio.moviefinder.ui.bases

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

open class BaseViewModel<T : BaseState> : ViewModel() {
    protected val _state: MutableLiveData<T> = MutableLiveData()
    internal val state: LiveData<T>
        get() = _state
}