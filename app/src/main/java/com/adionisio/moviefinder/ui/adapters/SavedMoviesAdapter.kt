package com.adionisio.moviefinder.ui.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.adionisio.moviefinder.databinding.MovieSavedItemViewHolderBinding
import com.adionisio.moviefinder.ui.extensions.loadMovieFromUrl
import com.adionisio.moviefinder.ui.models.MovieUI

class SavedMoviesAdapter(private val listener: (MovieUI) -> Unit) : RecyclerView.Adapter<SavedMoviesAdapter.SavedMoviesViewHolder>() {

    private val differCallback = object : DiffUtil.ItemCallback<MovieUI>() {
        override fun areItemsTheSame(oldItem: MovieUI, newItem: MovieUI): Boolean {
            return oldItem.title  == newItem.title
        }

        override fun areContentsTheSame(oldItem: MovieUI, newItem: MovieUI): Boolean {
            return oldItem == newItem
        }

    }

    val differ = AsyncListDiffer(this, differCallback)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SavedMoviesViewHolder {
        val binding = MovieSavedItemViewHolderBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return SavedMoviesViewHolder(binding, listener)
    }

    override fun getItemCount(): Int {
        return differ.currentList.size
    }

    override fun onBindViewHolder(holder: SavedMoviesViewHolder, position: Int) {
        holder.bind(differ.currentList[position])
    }

    class SavedMoviesViewHolder(private val binding: MovieSavedItemViewHolderBinding, private val listener: (MovieUI) -> Unit) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(movie: MovieUI) {
            binding.apply {
                imagePosterMovie.loadMovieFromUrl(movie.posterPath)
                root.setOnClickListener { listener(movie) }
                movieTitle.text = movie.title
            }
        }
    }

}