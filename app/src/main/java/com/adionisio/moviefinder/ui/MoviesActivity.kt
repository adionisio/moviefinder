package com.adionisio.moviefinder.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupWithNavController
import com.adionisio.moviefinder.R
import com.adionisio.moviefinder.databinding.ActivityMoviesBinding
import com.adionisio.moviefinder.ui.support.ToolbarHandler
import kotlinx.android.synthetic.main.activity_movies.*

class MoviesActivity : AppCompatActivity() {

    lateinit var binding: ActivityMoviesBinding
    lateinit var toolbarHandler: ToolbarHandler

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMoviesBinding.inflate(layoutInflater)
        setContentView(binding.root)
        toolbarHandler = ToolbarHandler(
            binding.toolbar,
            binding.toolbarTextView
        )
        val appBarConfiguration = AppBarConfiguration(setOf(R.id.homeMoviesFragment, R.id.savedMoviesFragment))
        val navController = findNavController(R.id.moviesNavHostFragment)
        val toolbar = binding.toolbar
        toolbar.setupWithNavController(navController, appBarConfiguration)
        bottomNavigationView.setupWithNavController(moviesNavHostFragment.findNavController())
    }
}