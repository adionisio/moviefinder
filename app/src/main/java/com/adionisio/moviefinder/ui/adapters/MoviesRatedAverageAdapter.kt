package com.adionisio.moviefinder.ui.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.adionisio.moviefinder.databinding.MovieSortedItemViewHolderBinding
import com.adionisio.moviefinder.ui.extensions.loadMovieCircleFromUrl
import com.adionisio.moviefinder.ui.models.MovieUI

class MoviesRatedAverageAdapter(private val listener: (MovieUI) -> Unit) : RecyclerView.Adapter<MoviesRatedAverageAdapter.MoviesViewHolder>() {

    private val differCallback = object : DiffUtil.ItemCallback<MovieUI>() {
        override fun areItemsTheSame(oldItem: MovieUI, newItem: MovieUI): Boolean {
            return oldItem.title  == newItem.title
        }

        override fun areContentsTheSame(oldItem: MovieUI, newItem: MovieUI): Boolean {
            return oldItem == newItem
        }

    }

    val differ = AsyncListDiffer(this, differCallback)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MoviesViewHolder {
        val binding = MovieSortedItemViewHolderBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return MoviesViewHolder(binding, listener)
    }

    override fun getItemCount(): Int {
        return differ.currentList.size
    }

    override fun onBindViewHolder(holder: MoviesViewHolder, position: Int) {
        holder.bind(differ.currentList[position])
    }

    class MoviesViewHolder(private val binding: MovieSortedItemViewHolderBinding, private val listener: (MovieUI) -> Unit) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(movie: MovieUI) {
            binding.apply {
                imagePosterMovie.loadMovieCircleFromUrl(movie.posterPath)
                voteAverage.text = movie.voteAverage.toString()
                imagePosterMovie.setOnClickListener { listener(movie) }
            }
        }
    }

}