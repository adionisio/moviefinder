package com.adionisio.moviefinder.ui.feats.saved

import android.os.Parcelable
import androidx.lifecycle.viewModelScope
import com.adionisio.moviefinder.Error
import com.adionisio.moviefinder.MovieRepository
import com.adionisio.moviefinder.ui.bases.BaseState
import com.adionisio.moviefinder.ui.bases.BaseViewModel
import com.adionisio.moviefinder.ui.models.MovieUI
import com.adionisio.moviefinder.ui.models.mapper.MapperUI.moviesUImapperFromMoviesDomain
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

class SavedMoviesViewModel @Inject constructor(private val repository: MovieRepository) :
    BaseViewModel<SavedMoviesViewModel.SavedMoviesState>() {

    var stateListSaved: Parcelable? = null

    data class SavedMoviesState(
        var loading: Boolean = false,
        var savedMoviesList: List<MovieUI> = mutableListOf(),
        var notFoundError: Boolean = false,
        var error: Boolean = false
    ) : BaseState

    init {
        _state.value = SavedMoviesState()
        getSavedMovies()
    }

    private fun getSavedMovies() {
        _state.value = _state.value?.copy(loading = true)
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                val result = repository.getSavedMovies()
                result.fold({ handleError(it) }) { movies ->
                    _state.postValue(
                        _state.value?.copy(
                            loading = false,
                            savedMoviesList = movies.map { moviesUImapperFromMoviesDomain(it) })
                    )
                }
            }
        }
    }

    private fun handleError(error: Error) {
        when (error) {
            Error.NotFound -> _state.postValue(
                _state.value?.copy(loading = false, notFoundError = true))
            else -> _state.postValue(_state.value?.copy(loading = false, error = true))
        }
    }


}