package com.adionisio.moviefinder.ui.support

import android.view.View
import android.widget.TextView
import androidx.appcompat.widget.Toolbar
import com.adionisio.moviefinder.R


class ToolbarHandler(private val toolbar: Toolbar, private val toolbarTextView: TextView) {

    fun showToolbar() {
        toolbar.visibility = View.VISIBLE
    }

    fun hideToolbar(){
        toolbar.visibility = View.GONE
    }

    fun setToolbarText(title: String) {
        toolbarTextView.apply {
            text = title
            visibility = View.VISIBLE
        }
    }

    fun hideToolbarLogo(){
        toolbarTextView.visibility = View.GONE
    }

    fun showToolbarLogo(){
        toolbarTextView.apply {
            text = this.context.getString(R.string.app_name)
            setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_saved_movies, 0, 0, 0);
            compoundDrawables[0].setTint(toolbarTextView.context.getColor(R.color.colorAccent))
            visibility = View.VISIBLE
        }
    }

}