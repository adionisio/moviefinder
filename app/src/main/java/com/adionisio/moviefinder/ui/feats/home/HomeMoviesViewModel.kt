package com.adionisio.moviefinder.ui.feats.home

import android.os.Parcelable
import androidx.lifecycle.viewModelScope
import com.adionisio.moviefinder.Error
import com.adionisio.moviefinder.MovieRepository
import com.adionisio.moviefinder.ui.bases.BaseState
import com.adionisio.moviefinder.ui.bases.BaseViewModel
import com.adionisio.moviefinder.ui.models.MovieUI
import com.adionisio.moviefinder.ui.models.mapper.MapperUI.moviesUImapperFromMoviesDomain
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import javax.inject.Inject

class HomeMoviesViewModel @Inject constructor(private val repository: MovieRepository) :
    BaseViewModel<HomeMoviesViewModel.HomeState>() {


    var stateListPopular: Parcelable? = null
    var stateListMostRated: Parcelable? = null
    var stateListUpcoming: Parcelable? = null
    private var pagePopularMovies: Int = 1
    private var pageMostRatedMovies: Int = 1
    private var pageUpcomingMovies: Int = 1

    data class HomeState(
        var loadingMostRated: Boolean = false,
        var loadingPopular: Boolean = false,
        var loadingUpcoming: Boolean = false,
        var loadingRandomPopularMovie: Boolean = false,
        var popularMoviesList: List<MovieUI> = mutableListOf(),
        var moviesMostRatedList: List<MovieUI> = mutableListOf(),
        var moviesUpcomingList: List<MovieUI> = mutableListOf(),
        var randomPopularMovie: MovieUI? = null,
        var errorMoviesNotFound: Boolean = false,
        var errorNetworkUnavailable: Boolean = false
    ) : BaseState

    init {
        _state.value = HomeState()
        getInitialData()
    }

    private fun getInitialData() {
        _state.value = _state.value?.copy(loadingPopular = true, loadingMostRated = true, loadingUpcoming = true)
        viewModelScope.launch {
            val resultPopularMovies = async { repository.getPopularMovies() }
            val resultTopRatedMovies = async { repository.getMostRatedMovies() }
            val resultUpcomingMovies = async { repository.getUpcomingMovies() }
            resultPopularMovies.await().fold(::handleFailure) { movies ->
                val moviesUI = movies.map { moviesUImapperFromMoviesDomain(it) }
                _state.value = _state.value?.copy(loadingPopular = false, popularMoviesList = moviesUI, randomPopularMovie = moviesUI.random())
            }
            resultTopRatedMovies.await().fold(::handleFailure) { movies ->
                val moviesUI = movies.map { moviesUImapperFromMoviesDomain(it) }
                val mostRatedMoviesSorted = moviesUI.filter { it.voteCount > 100 }.sortedWith(compareBy { it.voteAverage }).reversed()
                _state.value = _state.value?.copy(loadingMostRated = false, moviesMostRatedList = mostRatedMoviesSorted)
            }
            resultUpcomingMovies.await().fold(::handleFailure) { movies ->
                val moviesUI = movies.map { moviesUImapperFromMoviesDomain(it) }
                _state.value = _state.value?.copy(loadingUpcoming = false, moviesUpcomingList = moviesUI)
            }
        }
    }

    fun getMorePopularMovies(){
        _state.value = _state.value?.copy(loadingPopular = true)
        viewModelScope.launch {
            pagePopularMovies++
            val resultPopularMovies = repository.getPopularMovies(pagePopularMovies)
            resultPopularMovies.fold({ _state.value = _state.value?.copy(loadingPopular = false)}) { movies ->
                val moviesUI = movies.map { moviesUImapperFromMoviesDomain(it) }
                val mutableList = mutableListOf<MovieUI>()
                mutableList.addAll( _state.value?.popularMoviesList!!.toMutableList())
                mutableList.addAll(moviesUI)
                _state.value = _state.value?.copy(loadingPopular = false, popularMoviesList = mutableList)
            }
        }
    }

    fun getMostRatedMovies(){
        _state.value = _state.value?.copy(loadingMostRated = true)
        viewModelScope.launch {
            pageMostRatedMovies++
            val resultTopRatedMovies = repository.getMostRatedMovies(pageMostRatedMovies)
            resultTopRatedMovies.fold({ _state.value = _state.value?.copy(loadingMostRated = false)}) { movies ->
                val moviesUI = movies.map { moviesUImapperFromMoviesDomain(it) }
                val mutableList = mutableListOf<MovieUI>()
                mutableList.addAll( _state.value?.moviesMostRatedList!!.toMutableList())
                mutableList.addAll(moviesUI.filter { it.voteCount > 100 }.sortedWith(compareBy { it.voteAverage }).reversed())
                _state.value = _state.value?.copy(loadingMostRated = false, moviesMostRatedList = mutableList)
            }
        }
    }

    fun getUpcomingMovies(){
        _state.value = _state.value?.copy(loadingUpcoming = true)
        viewModelScope.launch {
            pageUpcomingMovies++
            val resultUpcomingMovies = repository.getUpcomingMovies(pageUpcomingMovies)
            resultUpcomingMovies.fold({ _state.value = _state.value?.copy(loadingUpcoming = false)}) { movies ->
                val moviesUI = movies.map { moviesUImapperFromMoviesDomain(it) }
                val mutableList = mutableListOf<MovieUI>()
                mutableList.addAll( _state.value?.moviesUpcomingList!!.toMutableList())
                mutableList.addAll(moviesUI)
                _state.value = _state.value?.copy(loadingUpcoming = false, moviesUpcomingList = mutableList)
            }
        }
    }

    private fun handleFailure(error: Error) {
        when (error) {
            is Error.NetworkUnavailable -> _state.value =
                _state.value?.copy(errorNetworkUnavailable = true)
            is Error.NotFound,
            is Error.ClientError,
            is Error.ServerError,
            is Error.UnexpectedError -> _state.value =
                _state.value?.copy(errorMoviesNotFound = true)
        }
    }

}