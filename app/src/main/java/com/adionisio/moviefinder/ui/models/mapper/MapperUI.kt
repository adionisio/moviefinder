package com.adionisio.moviefinder.ui.models.mapper

import com.adionisio.moviefinder.domain.Movie
import com.adionisio.moviefinder.ui.models.MovieUI

object MapperUI {

    fun moviesUImapperFromMoviesDomain(movie: Movie): MovieUI {
        return MovieUI(movie.id, movie.title, movie.overview, movie.posterPath, movie.voteAverage, movie.releaseDate, movie.voteCount, movie.adult)
    }

    fun moviesUImapperToMoviesDomain(movieUI: MovieUI): Movie {
        return Movie(movieUI.id, movieUI.title, movieUI.overview, movieUI.posterPath, movieUI.voteAverage, movieUI.releaseDate, movieUI.voteCount, movieUI.adult)
    }
}