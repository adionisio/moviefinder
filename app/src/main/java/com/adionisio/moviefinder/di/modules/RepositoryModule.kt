package com.adionisio.moviefinder.di.modules

import com.adionisio.moviefinder.MovieRepository
import com.adionisio.moviefinder.MovieRepositoryImpl
import com.adionisio.moviefinder.data.source.LocalDataSource
import com.adionisio.moviefinder.data.source.NetDataSource
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class RepositoryModule {

    @Singleton
    @Provides
    fun providesRepository(
        netDataSource: NetDataSource,
        localDataSource: LocalDataSource
    ): MovieRepository = MovieRepositoryImpl(netDataSource, localDataSource)

}