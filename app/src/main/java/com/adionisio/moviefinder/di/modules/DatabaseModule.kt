package com.adionisio.moviefinder.di.modules

import android.content.Context
import androidx.room.Room
import com.adionisio.moviefinder.data.source.local.db.room.MovieDatabaseDAO
import com.adionisio.moviefinder.data.source.local.db.room.MovieFinderDatabase
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class DatabaseModule {

    @Singleton
    @Provides
    fun provideMovieFinderDatabase(context: Context): MovieFinderDatabase {
        return Room.databaseBuilder(context, MovieFinderDatabase::class.java, "moviesfinder_database").build()
    }

    @Singleton
    @Provides
    fun provideMovieDatabaseDAO(movieFinderDatabase: MovieFinderDatabase): MovieDatabaseDAO {
        return movieFinderDatabase.movieDatabaseDAO
    }

}