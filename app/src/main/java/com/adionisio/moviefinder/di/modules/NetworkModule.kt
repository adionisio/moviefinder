package com.adionisio.moviefinder.di.modules

import android.content.Context
import com.adionisio.moviefinder.config.ConfigConstants
import com.adionisio.moviefinder.data.ConnectivityManagerImpl
import com.adionisio.moviefinder.data.source.ConnectivityManager
import com.adionisio.moviefinder.data.source.net.MoviesAPI
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
class NetworkModule {

    @Singleton
    @Provides
    fun providesGson(): Gson = GsonBuilder().create()

    @Singleton
    @Provides
    fun providesOkHttpClient(): OkHttpClient {
        val logging = HttpLoggingInterceptor().apply { level = HttpLoggingInterceptor.Level.BODY }
        return OkHttpClient().newBuilder().addInterceptor(logging).build()
    }

    @Singleton
    @Provides
    fun providesRetrofit(gson: Gson, okHttpClient: OkHttpClient): Retrofit {
        return Retrofit.Builder()
            .baseUrl(ConfigConstants.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .client(okHttpClient)
            .build()
    }

    @Singleton
    @Provides
    fun providesMoviesAPI(retrofit: Retrofit) : MoviesAPI {
        return retrofit.create(MoviesAPI::class.java)
    }

    @Singleton
    @Provides
    fun providesConnectivityManager(context: Context) : ConnectivityManager {
        return ConnectivityManagerImpl(context)
    }

}