package com.adionisio.moviefinder.di.components

import android.content.Context
import com.adionisio.moviefinder.di.modules.DataSourceModule
import com.adionisio.moviefinder.di.modules.DatabaseModule
import com.adionisio.moviefinder.di.modules.NetworkModule
import com.adionisio.moviefinder.di.modules.RepositoryModule
import com.adionisio.moviefinder.ui.feats.details.MovieDetailFragment
import com.adionisio.moviefinder.ui.feats.home.HomeMoviesFragment
import com.adionisio.moviefinder.ui.feats.saved.SavedMoviesFragment
import dagger.BindsInstance
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [DataSourceModule::class, NetworkModule::class, DatabaseModule::class, RepositoryModule::class])
interface AppComponent {

    @Component.Factory
    interface Factory {
        fun create(@BindsInstance context: Context): AppComponent
    }

    fun inject(fragment: HomeMoviesFragment)
    fun inject(fragment: MovieDetailFragment)
    fun inject(fragment: SavedMoviesFragment)
}