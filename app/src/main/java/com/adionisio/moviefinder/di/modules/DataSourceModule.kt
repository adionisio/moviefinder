package com.adionisio.moviefinder.di.modules

import com.adionisio.moviefinder.data.source.ConnectivityManager
import com.adionisio.moviefinder.data.source.LocalDataSource
import com.adionisio.moviefinder.data.source.NetDataSource
import com.adionisio.moviefinder.data.source.local.LocalDataSourceImpl
import com.adionisio.moviefinder.data.source.local.db.room.MovieDatabaseDAO
import com.adionisio.moviefinder.data.source.net.MoviesAPI
import com.adionisio.moviefinder.data.source.net.NetDataSourceImpl
import dagger.Module
import dagger.Provides

@Module
class DataSourceModule {

    @Provides
    fun provideNetDataSource(connectivityManager: ConnectivityManager, moviesAPI: MoviesAPI): NetDataSource {
        return NetDataSourceImpl(connectivityManager, moviesAPI)
    }

    @Provides
    fun provideLocalDataSource(movieFinderDatabase: MovieDatabaseDAO): LocalDataSource {
        return LocalDataSourceImpl(movieFinderDatabase)
    }

}