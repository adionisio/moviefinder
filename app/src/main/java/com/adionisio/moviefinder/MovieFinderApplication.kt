package com.adionisio.moviefinder

import android.app.Application
import com.adionisio.moviefinder.config.LocaleManager.setCurrentLocale
import com.adionisio.moviefinder.di.components.AppComponent
import com.adionisio.moviefinder.di.components.DaggerAppComponent


class MovieFinderApplication: Application() {

    val appComponent: AppComponent by lazy {
        DaggerAppComponent.factory().create(applicationContext)
    }

    override fun onCreate() {
        super.onCreate()
        initConfig()
    }

    private fun initConfig() {
        setCurrentLocale(applicationContext)
    }

}