package com.adionisio.moviefinder.config

import com.adionisio.moviefinder.BuildConfig

object ConfigConstants {
    const val BASE_URL = "https://api.themoviedb.org/"
    const val BASE_IMAGE_URL = "https://image.tmdb.org/t/p/w500"
    const val TMDB_API_KEY = BuildConfig.TMDB_API_KEY
    const val YOUTUBE_BASE_URL = "https://www.youtube.com/watch?v="
    const val IMDB_BASE_URL = "https://www.imdb.com/title/"
}