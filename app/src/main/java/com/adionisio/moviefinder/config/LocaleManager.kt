package com.adionisio.moviefinder.config

import android.content.Context
import android.os.Build

object LocaleManager {

    lateinit var locale: String

    fun setCurrentLocale(context: Context) {
        locale = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            context.resources.configuration.locales[0].language
        } else {
            context.resources.configuration.locale.language
        }
    }
}