package com.adionisio.moviefinder.data.source.net.models

import com.adionisio.moviefinder.config.ConfigConstants
import com.adionisio.moviefinder.domain.MovieDetails
import com.google.gson.annotations.SerializedName
import java.util.*

data class MovieDetailsNet(
    val adult: Boolean,
    @SerializedName("backdrop_path") val backdropPath: String,
    @SerializedName("belongs_to_collection") val belongsToCollection: Any,
    val budget: Int,
    val genres: List<Genre>,
    val homepage: String,
    val id: Int,
    @SerializedName("imdb_id") val imdbId: String,
    @SerializedName("original_language") val originalLanguage: String,
    @SerializedName("original_title") val originalTitle: String,
    val overview: String,
    val popularity: Double,
    @SerializedName("poster_path") val posterPath: String,
    @SerializedName("production_companies") val productionCompanies: List<ProductionCompany>,
    @SerializedName("production_countries") val productionCountries: List<ProductionCountry>,
    val release_date: Date,
    val revenue: Int,
    val runtime: Int,
    @SerializedName("spoken_languages") val spokenLanguages: List<SpokenLanguage>,
    val status: String,
    val tagline: String,
    val title: String,
    val video: Boolean,
    val videos: Videos,
    @SerializedName("vote_average") val voteAverage: Double,
    @SerializedName("vote_count") val voteCount: Int
)

fun MovieDetailsNet.toDomain(): MovieDetails {
    return MovieDetails(
        genres.map { it.name },
        homepage,
        id,
        ConfigConstants.IMDB_BASE_URL + imdbId,
        originalLanguage,
        originalTitle,
        runtime,
        spokenLanguages.map { it.name },
        tagline,
        ConfigConstants.YOUTUBE_BASE_URL + videos.resultsVideos.filter { it.site == "YouTube" }.map { it.key }.first()
    )
}