package com.adionisio.moviefinder.data.source.local.db.room

import com.adionisio.moviefinder.data.source.local.db.model.MovieDataDb
import com.adionisio.moviefinder.domain.Movie
import java.util.*

object MapperDb {

    fun movieFromDomainToDatabase(movie: Movie): MovieDataDb {
        return MovieDataDb(movie.id,
        movie.title,
        movie.overview,
        movie.posterPath,
        movie.voteAverage,
        movie.releaseDate,
        movie.voteCount,
        movie.adult,
        Date())
    }

    fun movieFromDatabaseToDomain(movie: MovieDataDb): Movie {
        return Movie(movie.id,
            movie.title,
            movie.overview,
            movie.posterPath,
            movie.voteAverage,
            movie.releaseDate,
            movie.voteCount,
            movie.adult)
    }
}