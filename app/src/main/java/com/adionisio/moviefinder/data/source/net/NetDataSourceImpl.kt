package com.adionisio.moviefinder.data.source.net

import com.adionisio.moviefinder.Error
import com.adionisio.moviefinder.data.source.ConnectivityManager
import com.adionisio.moviefinder.data.source.NetDataSource
import com.adionisio.moviefinder.data.source.net.models.MovieNet
import com.adionisio.moviefinder.data.source.net.models.toDomain
import com.adionisio.moviefinder.domain.Either
import com.adionisio.moviefinder.domain.Movie
import com.adionisio.moviefinder.domain.MovieDetails
import retrofit2.HttpException
import javax.inject.Inject

class NetDataSourceImpl @Inject constructor(
    private val connectivityManager: ConnectivityManager,
    private val moviesAPI: MoviesAPI
) : NetDataSource {

    override suspend fun getPopularMovies(page: Int): Either<Error, List<Movie>> {
        return if (connectivityManager.isConnected()) {
            try {
                val movies = moviesAPI.getPopularMovies(page = page.toString()).results
                processMovieListResult(movies)
            } catch (httpException: HttpException) {
                handleHttpException(httpException)
            } catch (exception: Exception) {
                Either.Left(Error.UnexpectedError)
            }
        } else {
            Either.Left(Error.NetworkUnavailable)
        }
    }

    override suspend fun getMostRatedMovies(page:Int): Either<Error, List<Movie>> {
        return if (connectivityManager.isConnected()) {
            try {
                val movies = moviesAPI.getTopRatedMovies(page = page.toString()).results
                processMovieListResult(movies)
            } catch (httpException: HttpException) {
                handleHttpException(httpException)
            } catch (exception: Exception) {
                Either.Left(Error.UnexpectedError)
            }
        } else {
            Either.Left(Error.NetworkUnavailable)
        }
    }

    override suspend fun getUpcomingMovies(page:Int): Either<Error, List<Movie>> {
        return if (connectivityManager.isConnected()) {
            try {
                val movies = moviesAPI.getUpcomingMovies(page = page.toString()).results
                processMovieListResult(movies)
            } catch (httpException: HttpException) {
                handleHttpException(httpException)
            } catch (exception: Exception) {
                Either.Left(Error.UnexpectedError)
            }
        } else {
            Either.Left(Error.NetworkUnavailable)
        }
    }

    override suspend fun getMovieDetails(movieId: Int): Either<Error, MovieDetails> {
        return if (connectivityManager.isConnected()) {
            try {
                val movie = moviesAPI.getMovieDetails(movieId = movieId)
                Either.Right(movie.toDomain())
            } catch (httpException: HttpException) {
                handleHttpException(httpException)
            } catch (exception: Exception) {
                Either.Left(Error.UnexpectedError)
            }
        } else {
            Either.Left(Error.NetworkUnavailable)
        }
    }

    private fun processMovieListResult(movies: List<MovieNet>): Either<Error, List<Movie>> {
        return if (movies.isNotEmpty()) {
            Either.Right(movies.map { it.toDomain() })
        } else {
            Either.Left(Error.NotFound)
        }
    }

    private fun handleHttpException(httpException: HttpException): Either.Left<Error> {
        return when (httpException.code()) {
            in 400..499 -> Either.Left(Error.ClientError)
            in 500..599 -> Either.Left(Error.ServerError)
            else -> Either.Left(Error.UnexpectedError)
        }
    }
}