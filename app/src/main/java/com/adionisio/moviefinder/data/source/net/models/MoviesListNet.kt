package com.adionisio.moviefinder.data.source.net.models

data class MoviesListNet(val results: List<MovieNet>)