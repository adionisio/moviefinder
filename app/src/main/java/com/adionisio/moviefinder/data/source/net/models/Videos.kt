package com.adionisio.moviefinder.data.source.net.models

import com.google.gson.annotations.SerializedName

data class Videos(
    @SerializedName("results") val resultsVideos: List<ResultVideo>
)