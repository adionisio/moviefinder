package com.adionisio.moviefinder.data.source.net

import com.adionisio.moviefinder.config.ConfigConstants.TMDB_API_KEY
import com.adionisio.moviefinder.config.LocaleManager
import com.adionisio.moviefinder.data.source.net.models.MovieDetailsNet
import com.adionisio.moviefinder.data.source.net.models.MovieNet
import com.adionisio.moviefinder.data.source.net.models.MoviesListNet
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface MoviesAPI {

    @GET("3/movie/popular")
    suspend fun getPopularMovies(
        @Query("api_key")
        apiKey: String = TMDB_API_KEY,
        @Query("language")
        language: String = LocaleManager.locale,
        @Query("page")
        page: String
    ): MoviesListNet

    @GET("3/movie/top_rated")
    suspend fun getTopRatedMovies(
        @Query("api_key")
        apiKey: String = TMDB_API_KEY,
        @Query("language")
        language: String = LocaleManager.locale,
        @Query("page")
        page: String
    ): MoviesListNet

    @GET("3/movie/upcoming")
    suspend fun getUpcomingMovies(
        @Query("api_key")
        apiKey: String = TMDB_API_KEY,
        @Query("language")
        language: String = LocaleManager.locale,
        @Query("page")
        page: String
    ): MoviesListNet

    @GET("3/movie/{movieId}")
    suspend fun getMovieDetails(
        @Path("movieId")
        movieId: Int,
        @Query("api_key")
        apiKey: String = TMDB_API_KEY,
        @Query("language")
        language: String = LocaleManager.locale,
        @Query("append_to_response")
        appendToResponse: String = "videos"
    ): MovieDetailsNet

}