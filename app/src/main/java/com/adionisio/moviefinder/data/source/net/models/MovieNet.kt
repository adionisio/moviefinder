package com.adionisio.moviefinder.data.source.net.models

import com.adionisio.moviefinder.domain.Movie
import com.google.gson.annotations.SerializedName
import java.util.*

data class MovieNet(
    val id: Int,
    val title: String,
    val overview: String,
    @SerializedName("poster_path") val posterPath: String,
    @SerializedName("vote_average") val voteAverage: Double,
    @SerializedName("release_date") val releaseDate: Date,
    @SerializedName("vote_count") val voteCount: Int,
    val adult: Boolean
)

fun MovieNet.toDomain(): Movie {
    return Movie(id, title, overview, posterPath, voteAverage, releaseDate, voteCount, adult)
}