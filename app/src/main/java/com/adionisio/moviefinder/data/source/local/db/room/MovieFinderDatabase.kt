package com.adionisio.moviefinder.data.source.local.db.room

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.adionisio.moviefinder.data.source.local.db.model.MovieDataDb

@Database(entities = [MovieDataDb::class], version = 1, exportSchema = false)
@TypeConverters(DateConverter::class)
abstract class MovieFinderDatabase : RoomDatabase() {

    abstract val movieDatabaseDAO: MovieDatabaseDAO

    companion object {
        @Volatile
        private var INSTANCE: MovieFinderDatabase? = null

        fun getInstance(context: Context): MovieFinderDatabase {
            synchronized(this) {
                var instance = INSTANCE
                if (instance == null) {
                    instance = Room.databaseBuilder(context.applicationContext, MovieFinderDatabase::class.java, "moviesfinder_database")
                        .fallbackToDestructiveMigration().build()
                    INSTANCE = instance
                }
                return instance
            }
        }
    }
}