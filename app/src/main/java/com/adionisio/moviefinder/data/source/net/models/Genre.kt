package com.adionisio.moviefinder.data.source.net.models

data class Genre(
    val id: Int,
    val name: String
)