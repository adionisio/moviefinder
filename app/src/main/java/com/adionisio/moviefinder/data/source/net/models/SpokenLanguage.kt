package com.adionisio.moviefinder.data.source.net.models

data class SpokenLanguage(
    val iso_639_1: String,
    val name: String
)