package com.adionisio.moviefinder.data.source.net.models

data class ProductionCountry(
    val iso_3166_1: String,
    val name: String
)