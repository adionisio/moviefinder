package com.adionisio.moviefinder.data.source.local

import com.adionisio.moviefinder.Error
import com.adionisio.moviefinder.data.source.LocalDataSource
import com.adionisio.moviefinder.data.source.local.db.room.MovieDatabaseDAO
import com.adionisio.moviefinder.data.source.local.db.room.MapperDb
import com.adionisio.moviefinder.domain.Either
import com.adionisio.moviefinder.domain.Movie
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.lang.Exception
import javax.inject.Inject

class LocalDataSourceImpl @Inject constructor(private val movieDatabase: MovieDatabaseDAO) : LocalDataSource{

    override fun getSavedMovies(): Either<Error, List<Movie>> {
        return try {
            val movies = movieDatabase.getAllMoviesByInsertDate()
                .map { MapperDb.movieFromDatabaseToDomain(it) }
            if (movies.isNotEmpty()) {
                Either.Right(movies)
            } else {
                Either.Left(Error.NotFound)
            }
        } catch (e: Exception) {
            Either.Left(Error.UnexpectedError)
        }
    }

    override fun saveMovie(movie: Movie) {
        movieDatabase.insert(MapperDb.movieFromDomainToDatabase(movie))
    }

    override fun removeMovie(movie: Movie) {
        movieDatabase.removeAllMovies()
    }
}