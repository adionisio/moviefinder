package com.adionisio.moviefinder.data.source.local.db.room

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.adionisio.moviefinder.data.source.local.db.model.MovieDataDb

@Dao
interface MovieDatabaseDAO{

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(movie: MovieDataDb)

    @Query("SELECT * from movies WHERE id = :id")
    fun getMovieById(id: String): MovieDataDb

    @Query("SELECT * from movies ORDER BY insert_date DESC")
    fun getAllMoviesByInsertDate(): List<MovieDataDb>

    @Query("DELETE FROM movies")
    fun removeAllMovies()

}