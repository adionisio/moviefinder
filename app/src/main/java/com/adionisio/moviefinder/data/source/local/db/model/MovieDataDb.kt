package com.adionisio.moviefinder.data.source.local.db.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.*

@Entity(tableName = "movies")
data class MovieDataDb (@PrimaryKey val id: Int,
                        val title: String,
                        val overview: String,
                        @ColumnInfo(name = "poster_path") val posterPath: String,
                        @ColumnInfo(name = "vote_average") val voteAverage: Double,
                        @ColumnInfo(name = "release_date") val releaseDate: Date,
                        @ColumnInfo(name = "vote_count") val voteCount: Int,
                        val adult: Boolean,
                        @ColumnInfo(name = "insert_date")val inserDate: Date){

}