package com.adionisio.moviefinder

import com.adionisio.moviefinder.data.source.LocalDataSource
import com.adionisio.moviefinder.data.source.NetDataSource
import com.adionisio.moviefinder.domain.Either
import com.adionisio.moviefinder.domain.Movie
import com.adionisio.moviefinder.fake.MoviesFake.DATE
import com.adionisio.moviefinder.fake.MoviesFake.LIST_MOVIE
import com.adionisio.moviefinder.fake.MoviesFake.MOVIE
import kotlinx.coroutines.runBlocking
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.Mockito.`when`
import org.mockito.Mockito.verify

class MovieRepositoryTest {

    @Mock
    lateinit var mockDataSource: NetDataSource
    @Mock
    lateinit var mockLocalDataSource: LocalDataSource

    lateinit var repository: MovieRepository

    @Before
    @Throws(Exception::class)
    fun setUp() {
        mockDataSource = Mockito.mock(NetDataSource::class.java)
        mockLocalDataSource = Mockito.mock(LocalDataSource::class.java)
        repository = MovieRepositoryImpl(mockDataSource, mockLocalDataSource)
    }

    @Test
    fun `Get popular movies from Net`() {
        runBlocking {
            `when`(mockDataSource.getPopularMovies()).thenReturn(Either.Right(LIST_MOVIE))
            val popularMovie = repository.getPopularMovies()
            popularMovie.fold({fail()}) { movie -> checkMovie(movie) }
        }
    }

    @Test
    fun `Get error empty popular movies from Net`() {
        runBlocking {
            `when`(mockDataSource.getPopularMovies()).thenReturn(Either.Left(Error.NotFound))
            when (val popularMovie = repository.getPopularMovies()) {
                is Either.Right -> fail()
                is Either.Left -> { assertTrue(popularMovie.error is Error.NotFound) }
            }
        }
    }

    @Test
    fun `Get error popular movies no internet error from Net`() {
        runBlocking {
            `when`(mockDataSource.getPopularMovies()).thenReturn(Either.Left(Error.NetworkUnavailable))
            when (val popularMovie = repository.getPopularMovies()) {
                is Either.Right -> fail()
                is Either.Left -> { assertTrue(popularMovie.error is Error.NetworkUnavailable) }
            }
        }
    }

    @Test
    fun `Get error popular movies unexpected error from Net`() {
        runBlocking {
            `when`(mockDataSource.getPopularMovies()).thenReturn(Either.Left(Error.UnexpectedError))
            when (val popularMovie = repository.getPopularMovies()) {
                is Either.Right -> fail()
                is Either.Left -> { assertTrue(popularMovie.error is Error.UnexpectedError) }
            }
        }
    }

    @Test
    fun `Get error popular movies client server error from Net`() {
        runBlocking {
            `when`(mockDataSource.getPopularMovies()).thenReturn(Either.Left(Error.ClientError))
            when (val popularMovie = repository.getPopularMovies()) {
                is Either.Right -> fail()
                is Either.Left -> { assertTrue(popularMovie.error is Error.ClientError) }
            }
        }
    }

    @Test
    fun `Get error popular movies server error from Net`() {
        runBlocking {
            `when`(mockDataSource.getPopularMovies()).thenReturn(Either.Left(Error.ServerError))
            when (val popularMovie = repository.getPopularMovies()) {
                is Either.Right -> fail()
                is Either.Left -> { assertTrue(popularMovie.error is Error.ServerError) }
            }
        }
    }

    @Test
    fun `Get saved movies from local storage`() {
        runBlocking {
            runBlocking {
                `when`(mockLocalDataSource.getSavedMovies()).thenReturn(Either.Right(LIST_MOVIE))
                val popularMovie = repository.getSavedMovies()
                popularMovie.fold({fail()}) { movie -> checkMovie(movie) }
            }
        }
    }

    @Test
    fun `Get error empty saved movies from local storage`() {
        runBlocking {
            `when`(mockLocalDataSource.getSavedMovies()).thenReturn(Either.Left(Error.NotFound))
            when (val savedMovies = repository.getSavedMovies()) {
                is Either.Right -> fail()
                is Either.Left -> { assertTrue(savedMovies.error is Error.NotFound) }
            }
        }
    }

    @Test
    fun `Save movie to local storage`() {
        repository.saveMovie(MOVIE)
        verify(mockLocalDataSource).saveMovie(MOVIE)
    }

    @Test
    fun `Remove movie from local storage`() {
        repository.removeMovie(MOVIE)
        verify(mockLocalDataSource).removeMovie(MOVIE)
    }

    @Test
    fun `Get top rated movies from Net`() {
        runBlocking {
            `when`(mockDataSource.getMostRatedMovies()).thenReturn(Either.Right(LIST_MOVIE))
            val lastMoviesResult = repository.getMostRatedMovies()
            lastMoviesResult.fold({fail()}) { movie -> checkMovie(movie) }
        }
    }

    @Test
    fun `Get error empty top rated movies from Net`() {
        runBlocking {
            `when`(mockDataSource.getMostRatedMovies()).thenReturn(Either.Left(Error.NotFound))
            when (val lastMoviesResult = repository.getMostRatedMovies()) {
                is Either.Right -> fail()
                is Either.Left -> { assertTrue(lastMoviesResult.error is Error.NotFound) }
            }
        }
    }

    @Test
    fun `Get internet connection error top rated movies from Net`() {
        runBlocking {
            `when`(mockDataSource.getMostRatedMovies()).thenReturn(Either.Left(Error.NetworkUnavailable))
            when (val lastMoviesResult = repository.getMostRatedMovies()) {
                is Either.Right -> fail()
                is Either.Left -> { assertTrue(lastMoviesResult.error is Error.NetworkUnavailable) }
            }
        }
    }

    @Test
    fun `Get client server error top rated movies from Net`() {
        runBlocking {
            `when`(mockDataSource.getMostRatedMovies()).thenReturn(Either.Left(Error.ClientError))
            when (val lastMoviesResult = repository.getMostRatedMovies()) {
                is Either.Right -> fail()
                is Either.Left -> { assertTrue(lastMoviesResult.error is Error.ClientError) }
            }
        }
    }

    @Test
    fun `Get server error top rated movies from Net`() {
        runBlocking {
            `when`(mockDataSource.getMostRatedMovies()).thenReturn(Either.Left(Error.ServerError))
            when (val lastMoviesResult = repository.getMostRatedMovies()) {
                is Either.Right -> fail()
                is Either.Left -> { assertTrue(lastMoviesResult.error is Error.ServerError) }
            }
        }
    }

    @Test
    fun `Get upcoming movies from Net`() {
        runBlocking {
            `when`(mockDataSource.getUpcomingMovies()).thenReturn(Either.Right(LIST_MOVIE))
            val upcomingMoviesResult = repository.getUpcomingMovies()
            upcomingMoviesResult.fold({fail()}) { movie -> checkMovie(movie) }
        }
    }

    @Test
    fun `Get error empty upcoming movies from Net`() {
        runBlocking {
            `when`(mockDataSource.getUpcomingMovies()).thenReturn(Either.Left(Error.NotFound))
            when (val upcomingMovies = repository.getUpcomingMovies()) {
                is Either.Right -> fail()
                is Either.Left -> { assertTrue(upcomingMovies.error is Error.NotFound) }
            }
        }
    }

    @Test
    fun `Get error internet connection upcoming movies from Net`() {
        runBlocking {
            `when`(mockDataSource.getUpcomingMovies()).thenReturn(Either.Left(Error.NetworkUnavailable))
            when (val upcomingMovies = repository.getUpcomingMovies()) {
                is Either.Right -> fail()
                is Either.Left -> { assertTrue(upcomingMovies.error is Error.NetworkUnavailable) }
            }
        }
    }

    @Test
    fun `Get error client server upcoming movies from Net`() {
        runBlocking {
            `when`(mockDataSource.getUpcomingMovies()).thenReturn(Either.Left(Error.ClientError))
            when (val upcomingMovies = repository.getUpcomingMovies()) {
                is Either.Right -> fail()
                is Either.Left -> { assertTrue(upcomingMovies.error is Error.ClientError) }
            }
        }
    }

    @Test
    fun `Get error server upcoming movies from Net`() {
        runBlocking {
            `when`(mockDataSource.getUpcomingMovies()).thenReturn(Either.Left(Error.ServerError))
            when (val upcomingMovies = repository.getUpcomingMovies()) {
                is Either.Right -> fail()
                is Either.Left -> { assertTrue(upcomingMovies.error is Error.ServerError) }
            }
        }
    }

    private fun checkMovie(movie: List<Movie>) {
        assertEquals("", movie.first().title)
        assertEquals("", movie.first().overview)
        assertEquals("", movie.first().posterPath)
        assertEquals("", 0.0, movie.first().voteAverage, 0.0)
        assertEquals(DATE, movie.first().releaseDate)
    }
}