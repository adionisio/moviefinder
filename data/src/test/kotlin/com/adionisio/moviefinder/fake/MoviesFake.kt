package com.adionisio.moviefinder.fake

import com.adionisio.moviefinder.domain.Movie
import java.util.*

object MoviesFake {

    val DATE = Date()
    val MOVIE = Movie(0,"", "", "", 0.0, DATE, 0, false)
    val LIST_MOVIE = listOf(MOVIE)
}