package com.adionisio.moviefinder

import com.adionisio.moviefinder.data.source.LocalDataSource
import com.adionisio.moviefinder.data.source.NetDataSource
import com.adionisio.moviefinder.domain.Either
import com.adionisio.moviefinder.domain.Movie
import com.adionisio.moviefinder.domain.MovieDetails

interface MovieRepository {
    suspend fun getPopularMovies(page: Int = 1): Either<Error, List<Movie>>
    suspend fun getMostRatedMovies(page: Int = 1): Either<Error, List<Movie>>
    suspend fun getUpcomingMovies(page: Int = 1): Either<Error, List<Movie>>
    suspend fun getMovieDetails(movieId: Int): Either<Error, MovieDetails>
    suspend fun getSavedMovies(): Either<Error, List<Movie>>
    fun saveMovie(movie: Movie)
    fun removeMovie(movie: Movie)
}

class MovieRepositoryImpl(private val netDataSource: NetDataSource,
                          private val localDataSource: LocalDataSource
) : MovieRepository {

    override suspend fun getPopularMovies(page: Int): Either<Error, List<Movie>> = netDataSource.getPopularMovies(page)
    override suspend fun getMostRatedMovies(page: Int): Either<Error, List<Movie>> = netDataSource.getMostRatedMovies(page)
    override suspend fun getUpcomingMovies(page: Int): Either<Error, List<Movie>> = netDataSource.getUpcomingMovies(page)
    override suspend fun getMovieDetails(movieId: Int): Either<Error, MovieDetails> = netDataSource.getMovieDetails(movieId)
    override suspend fun getSavedMovies(): Either<Error, List<Movie>> = localDataSource.getSavedMovies()
    override fun saveMovie(movie: Movie) = localDataSource.saveMovie(movie)
    override fun removeMovie(movie: Movie) = localDataSource.removeMovie(movie)
}

