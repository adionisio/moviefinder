package com.adionisio.moviefinder.data.source

interface ConnectivityManager {

    fun isConnected(): Boolean

}