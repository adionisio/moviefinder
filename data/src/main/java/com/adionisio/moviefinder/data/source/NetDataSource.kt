package com.adionisio.moviefinder.data.source

import com.adionisio.moviefinder.Error
import com.adionisio.moviefinder.domain.Either
import com.adionisio.moviefinder.domain.Movie
import com.adionisio.moviefinder.domain.MovieDetails

interface NetDataSource {
    suspend fun getPopularMovies(page:Int = 1): Either<Error, List<Movie>>
    suspend fun getMostRatedMovies(page:Int = 1): Either<Error, List<Movie>>
    suspend fun getUpcomingMovies(page:Int = 1): Either<Error, List<Movie>>
    suspend fun getMovieDetails(movieId: Int): Either<Error, MovieDetails>
}