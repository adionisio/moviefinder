package com.adionisio.moviefinder.data.source

import com.adionisio.moviefinder.Error
import com.adionisio.moviefinder.domain.Either
import com.adionisio.moviefinder.domain.Movie

interface LocalDataSource {
    fun getSavedMovies(): Either<Error, List<Movie>>
    fun saveMovie(movie: Movie)
    fun removeMovie(movie: Movie)
}