package com.adionisio.moviefinder

sealed class Error{
    object NotFound : Error()
    object NetworkUnavailable : Error()
    object ClientError : Error()
    object ServerError : Error()
    object UnexpectedError : Error()
}
