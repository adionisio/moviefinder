# Movie Finder

Movie finder is an Android app to practice MVVM

## Installation

Set the Tmdb api-key in gradle.properties

```bash
tmdb_apy_key = "XXXXXXXXX"
```

## Usage

Clone the repository, add Tmdb api key to gradle, sync and rebuild it.

## Modules

The project has 3 modules (domain, data and app). App module has dendencies to data and domain. Data only has dependency with domain, and Domain has not dependencies.

## Libraries & Tools

- ViewBinding
- LiveData
- Coroutines
- Navigation Components
- Retrofit (Net request)
- Dagger2 (Dependency Injector)
- Glide (Images)
- Room (Database)

## Images

![Home](/images/home1.png)
![Home](/images/home2.png)
![Movie details](/images/detail.png)
![Saved movies](/images/saved.png)

## Architecture

![Architecture](/images/architecture.png)