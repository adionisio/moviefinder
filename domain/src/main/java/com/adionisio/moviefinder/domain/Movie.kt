package com.adionisio.moviefinder.domain

import java.util.*

data class Movie(
    val id: Int,
    val title: String,
    val overview: String,
    val posterPath: String,
    val voteAverage: Double,
    val releaseDate: Date,
    val voteCount: Int,
    val adult: Boolean
)