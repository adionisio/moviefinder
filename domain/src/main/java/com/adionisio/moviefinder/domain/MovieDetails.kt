package com.adionisio.moviefinder.domain


data class MovieDetails(
    val genres: List<String>,
    val homepage: String,
    val id: Int,
    val imdbId: String,
    val originalLanguage: String,
    val originalTitle: String,
    val runtime: Int,
    val spokenLanguages: List<String>,
    val tagline: String,
    val video: String?
)