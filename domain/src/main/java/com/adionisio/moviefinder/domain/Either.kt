package com.adionisio.moviefinder.domain

sealed class Either <out L, out R> {

    data class Left<out L>(val error: L) : Either<L, Nothing>()

    data class Right<out R>(val data: R) : Either<Nothing, R>()

    fun <T> fold(fnL: (L) -> T, fnR: (R) -> T): T {
        return when (this) {
            is Left -> fnL(error)
            is Right -> fnR(data)
        }
    }
}